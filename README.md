# Probability of Cosmogenic Nuclide Production Rates

MATLAB code for calculating a most probable production rate in your sample.

## Download

If you don’t have an account, you can download the files as a zip folder. This means if there are any changes in the files you have to re-download the files and cannot merge your personal changes. 

## Contribute

If you have a GitLab account, create your personal fork. This allows you to get all recent updates and bug fixes and incorporate personal changes into updated files. You can request to contribute to the project. 

## How to run

* To run the code you need the Leya & Masarik (2009) excel sheet for cosmogenic nuclide production rates. Input the chemistry for your sample in this file. 
* Specify the path to the excel file in the Productionrates.m file. You can specify additional parameters in this file, such as assumed % of atmospheric ablation, density of the sample, etc. 

## Authors

* Cornelia A. K. Mertens <cornelia.mertens@erdw.ethz.ch>
