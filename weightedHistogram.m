%The weighed histogram function is a bit complex. The problem is, that we
%cannot simply plot a distribution over all weighed Production rates, as
%these do not reflect which Production rate they originally belonged to. We
%need to define bins based on the actual production rates, and fill each
%bin with all production rates that fall inside its ranges, times its
%weighed. 

function [xval,HistVal]=weightedHistogram(WeighedProductionrates,ProductionRates,binwidth)
lower=min(ProductionRates); %find minimum and maximum production rates
upper=max(ProductionRates);
binno=ceil((upper-lower)/binwidth); %no of bins used for data
if ~any(binno)
    error('No production rates left! Consider excluding a smaller amount of atmosphere or radius or change shielding');
end
HistVal=zeros(binno,1);
xval=zeros(binno,1);
for n=1:binno
limlow=lower+(n-1)*binwidth;
limup=lower+n*binwidth;
HistVal(n)=sum(WeighedProductionrates(ProductionRates<limup));
WeighedProductionrates(ProductionRates<limup)=[];
ProductionRates(ProductionRates<limup)=[];
xval(n)=mean([limlow limup]);
end
end