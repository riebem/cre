
function [confi95,confi95plus,ProbableP] = histCIs(xval,bins,data)
normbins=bins/nansum(data)*100;
normbinscum=zeros(100,0);
for o=1:numel(normbins)
    normbinscum(o)=sum(normbins(1:o));
    normbinscumright(o)=sum(normbins(end-o+1:end));
end

%from the right side
find95confidenceplus=find(normbinscumright>2.5);
fxval=flip(xval);
confi95plus=fxval(find95confidenceplus(1));
if ~ismember(find95confidenceplus,1)
percentage=normbinscumright(find95confidenceplus(1)-1);
else
    percentage=0;
end
find95confidence=find(normbinscum>5-percentage);
confi95=xval(find95confidence(1));

ProbableP=sum(xval.*bins)/sum(bins);

end

