function PlotProduction(Rstep,allr,allint,colorspec,allr_unchanged,userlabel,confi95,confi95plus,ProbableP,legendon)

for a=1:numel(allr)
  if any(allint(:,a))
  p=plot(Rstep(:,a),allint(:,a),'DisplayName',num2str(allr(a)),'LineWidth',1,'Color',colorspec(find(allr_unchanged==allr(a)),:)); 
  hold on
  end
end
yline(nanmean(allint(:)),'-.k','DisplayName','mean');
yline(ProbableP,'Color',[0.85 0.325 0.098],'LineWidth',1.5,'DisplayName','most probable');
yline(confi95,'Color',[0 0.3 0.5,0.3],'LineStyle',':','LineWidth',1.,'DisplayName','95%CI');
yline(confi95plus,'Color',[0 0.3 0.5,0.3],'LineStyle',':','LineWidth',1.);

if any(legendon)
legend 
end
xlabel('depth')
ylabel(userlabel)

end
