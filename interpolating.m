function [allint,intnearest]=interpolating(dep,N,step)

TF=find(ischange(dep,'linear')); %find aprubt changes in the radius dataset
TF=[1;TF;numel(dep)+1];
stepno=linspace(0,1,step);
allint=zeros(step,numel(TF)-1);
intnearest=zeros(step,numel(TF)-1);
%figure('Name','Interpolated values')
for i=1:numel(TF)-1
    d=dep(TF(i):TF(i+1)-1); %depth
    n=N(TF(i):TF(i+1)-1); %Production rate bzw 22/21Ne
    r=dep(TF(i+1)-1);
    halfwidth=(d(2)-d(1))/2;
    int=interp1(d-halfwidth,n,r*stepno,'linear','extrap');
    intnear=interp1(d-halfwidth,n,r*stepno,'nearest','extrap');
    intnearest(:,i)=intnear;
    allint(:,i)=int;
    %plot(d-halfwidth,n,'x')
    % hold on
    %plot(r*stepno,int,'.')
end

% legend('Ingo Leyas data points','Interpolated values')
% xlabel('depth')
% ylabel('22Ne/21Ne')
end